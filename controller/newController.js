var getModel = require('../models/dbModelOrSchema');

module.exports  =  function(app){

    app.get('/api/setupJob', function(req, res){
        //inserting initial dataabse values
        var initialProfile = [
        {
             title: "software developer",
             company: "Homelogic",
             date: "1993-08-28",
             jobboard: "indeed",
             resume: true,
             link: "www.indeed.com",

        },

        {
             title: "Jr software developer",
             company: "George Mason University",
             date: "2016-08-15",
             jobboard: "hiremason",
             resume: true,
             link: "www.hiremason.com",
        }
        ];
        
        getModel.create(initialProfile, function(err,results){
            res.send(results);
        });

    });
}