var getModel1 = require('../models/dbModelOrSchema');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');


module.exports = function(app){

    app.use(bodyParser.json());  
    app.use(bodyParser.urlencoded({extended:true}));
    app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
    next();
    });

    app.get('/api/setupJob/:company' , function(req, res){
            getModel1.find({company: req.params.company},
            function(err, anyNameVarible){
                if(err) throw err;
                res.json(anyNameVarible);
            });
         });
     app.get('/api/setupJobs' , function(req, res){
            getModel1.find(function(err, anyNameVarible){
                if(err) throw err;
                res.json(anyNameVarible);
            });
         });
    app.get('/api/setupJobs/:id' , function(req, res){
            getModel1.findById(mongoose.Types.ObjectId(req.params.id),
            function(err, anyNameVariable){
                if(err) throw err;
                res.json(anyNameVariable);
            });
        });

    app.post('/api/setupJob', function(req,res){

        if(req.body.id)
            {
                getModel1.findByIdAndUpdate(mongoose.Types.ObjectId(req.body.id),{
                    title: req.body.title,
                    company: req.body.company,
                    date: req.body.date,
                    jobboard: req.body.jobboard,
                    resume: req.body.resume,
                    link: req.body.link,
                }, function(err, anyNameVariable){
                    if(err) throw err;
                    res.json(anyNameVariable);
                })
            }
        else
            {
                var newSetup = getModel1(
                    {
                    title: req.body.title,
                    company: req.body.company,
                    date: req.body.date,
                    jobboard: req.body.jobboard,
                    resume: req.body.resume,
                    link: req.body.link,
                    });
                
                newSetup.save(function(err, anyNameVariable){
                    if(err) throw err;
                    res.json(anyNameVariable);
                });
            }
    });
         
    app.delete('/api/setupJobDelete/:id', function(req,res)
        {
            getModel1.findByIdAndRemove( mongoose.Types.ObjectId(req.params.id) , 
            function(err, anyNameVariable){
                if(err) throw err;
                res.json(anyNameVariable);

        });
        });
}