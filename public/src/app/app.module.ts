import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule }    from '@angular/http';  
import { AppComponent }  from './app.component';
import { bunty } from './bunty.component'
import { FormsModule }   from '@angular/forms';


@NgModule({
  imports:      [ BrowserModule , HttpModule, FormsModule],
  declarations: [ AppComponent , bunty ],
  bootstrap:    [ AppComponent , bunty ]
})
export class AppModule { }


