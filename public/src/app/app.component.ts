import { Component } from '@angular/core';
import { NewJobService} from './services/newJob.service';
import { Jobs} from '../client';

@Component({
  moduleId: module.id,
  selector: 'my-app',
  templateUrl: 'app.component.html',
  providers:[NewJobService]

})


export class AppComponent  {
  jobs:Jobs[];
  company:string;
  title:string;
  date: Date;
  jobboard: string;
  resume: boolean;
  link: string;

   constructor(private newJobService:NewJobService){

  }

      addJob(event){
        event.preventDefault();

        
        var jobNew= {
          company:this.company,
          title:this.title,
          jobboard:this.jobboard,
          resume:false,
          date:this.date,
          link:this.link,
          applied:false
        }

         this.newJobService.addJob(jobNew).subscribe(job => {this.jobs=job ;});
         this.company='';
         this.title='';
         this.jobboard='';
         this.date= new Date(0);
         this.link='';

      }
}

//we use moduleID when we import templates from external file
//we use providers when injecting services like NewJobService

