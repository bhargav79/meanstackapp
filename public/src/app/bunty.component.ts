import { Component } from '@angular/core';
import { NewJobService} from './services/newJob.service';
import { Jobs} from '../client';

@Component({
  moduleId: module.id,
  selector: 'bunty',
  templateUrl: `bunty.component.html`,
  providers:[NewJobService]
})


export class bunty  { 

  jobs:Jobs[];
  company:string;
  title:string;
  date: Date;
  jobboard: string;
  resume: boolean;
  link: string;

  constructor(private newJobService:NewJobService){
    
  }


  getJob()
  {
    return this.newJobService.getJob().subscribe( job => {this.jobs=job;});
  }

  getJobByName(com)
  {
    return this.newJobService.getJobByName(com).subscribe( job => {this.jobs=job;});
  }

  deleteJob(id){
    var jobs = this.jobs;

    return this.newJobService.deleteJob(id).subscribe( data =>
    {
      for(var i= 0;i<jobs.length;i++)
        {
          if(jobs[i]._id == id)
            {
              jobs.splice(i,1);
            }
        }
    });
    
  }

}



//after injecting the serivice NewJobService we are subscribing the obeservable(google observables in reactivejs)

//we imported a Jobs[] class that we created outside of source folder(we can create anywhere)
//this client.ts which exports Jobs[] specifies the structure of our database document files