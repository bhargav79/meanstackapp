import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class NewJobService{
    constructor(private http:Http){
        
    }

    getJob()
    {
        return this.http.get('http://localhost:80/api/setupJobs' ).map(res => res.json());
    }

    getJobByName(com)
    {
        return this.http.get('http://localhost:80/api/setupJob/'+ com ).map(res => res.json());
    }

    addJob(jobNew)
    {
            var headers = new Headers();
            console.log(jobNew);
            headers.append('Content-Type','application/json');
            return this.http.post('http://localhost:80/api/setupJob' , JSON.stringify(jobNew) , {headers:headers}).map(res => res.json());
            //may be you should convert jobNew into string
    }

    deleteJob(id){
        return this.http.delete('http://localhost:80/api/setupJobDelete/' + id ).map( res => res.json());
    }
}



//this file is where we pass http request as an observable(this is how we send requests in Angular using reactive)
//Since it is cumbersome to create services for every component we create only one service and inject it into 
//all components


//An injectable service is nothing but reuable part of code that performs certain tasks for client.
//google dependency injection

//steps included in angular frontend building
//download angular into a folder. Here it is public
//check for app.module.ts
//check for app.component.ts.
//check for main.js, index.html, style.css , tsconfig.json, main.ts
//if you want to create your own component(eg <bunty></bunty>) copy app.component into your component and modify accordingly
//create a service file with can be injected into all components. xxxxx.servise.ts in separate folder
//whatever modules you are using, add them to app/module

//