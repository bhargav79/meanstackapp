export class Jobs
{
    title: string;
    company: string;
    date: Date;
    jobboard: string;
    resume: boolean;
    link: string;
    _id:string;
}