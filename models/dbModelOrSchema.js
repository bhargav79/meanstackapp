var mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
var Schema = mongoose.Schema;

var myAppSchema = new Schema({
    title: String,
    company: String,
    date: Date,
    jobboard: String,
    resume: Boolean,
    link: String,
});


var myApp = mongoose.model('myApp' , myAppSchema);

module.exports = myApp;