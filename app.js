var ex= require('express');
var app = ex();
var mongoose  = require('mongoose');
var config = require('./config');
var setupController= require('./controller/newController');
var port= process.env.PORT || 80;
var apiController = require('./controller/apiController');
app.use('/', ex.static(__dirname + '/public'));

app.set('view engine' , 'ejs');

setupController(app);
apiController(app);
mongoose.connect('mongodb://localhost:27017/newApp', { useMongoClient: true });
app.listen(port);

